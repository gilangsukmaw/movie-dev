const validator = require("validator");
const mongoose = require("mongoose");

exports.getGenreByIdValidator = async (req, res, next) => {
  try {
    if (!mongoose.Types.ObjectId.isValid(req.params.id)) {
      return next({ message: "id is not valid", statusCode: 400 });
    }
    next();
  } catch (error) {
    next(error);
  }
};

exports.cretaeOrUpdateGenreValidator = async (req, res, next) => {
  try {
    if (validator.isEmpty(req.body.genre)) {
      return next({ message: "Category must not be empty", statusCode: 400 });
    }
    next();
  } catch (error) {
    next(error);
  }
};
