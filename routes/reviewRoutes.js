const express = require("express");

// import auth

//import validator
const { getReviewValidator, createOrUpdateReviewValidator } = require("../middlewares/validators/reviewValidator");

//import constroller
const {
	createReview,
	getAllMoviesReviews,
	getAllUsersReviews,
	updateReview,
	deleteReview,
} = require("../controllers/reviewController");

const router = express.Router();

router
	.route("/:id_movie")
	.post(createOrUpdateReviewValidator, createReview)
	.get(getReviewValidator, getAllMoviesReviews);

router.route("/user/:id_user").get(getReviewValidator, getAllUsersReviews);

router.route("/:id").put(createOrUpdateReviewValidator, updateReview).delete(deleteReview);

module.exports = router;
