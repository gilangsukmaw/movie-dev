const express = require("express");

const { signup, signin, admin } = require("../middlewares/auth/auth");

const {
  signUpValidator,
  signInValidator,
  getUserByIdValidator,
} = require("../middlewares/validators/authValidator");

const {
  getToken,
  getCurrentUser,
  getAllUser,
  getUserById,
  updateUserById,
  deleteUserById,
} = require("../controllers/authController");

const router = express.Router();

router.get("/user/all", getAllUser);
router.get("/user/:id", getUserByIdValidator, getUserById);
router.get("/getcurrentuser", getCurrentUser);

router.post("/signup", signUpValidator, signup, getToken);
router.post("/signin", signInValidator, signin, getToken);

router.put("/user/:id", updateUserById);
router.delete("/user/:id", deleteUserById);

module.exports = router;
