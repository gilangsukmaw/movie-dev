const express = require("express");
const route = express.Router();

const {
  getAllGenre,
  getGenreById,
  createGenre,
  updateGenreById,
  deleteGenreById,
} = require("../controllers/genreController");

const {
  cretaeOrUpdateGenreValidator,
  getGenreByIdValidator,
} = require("../middlewares/validators/genreValidator");

route.get("/", getAllGenre);
route.get("/:id", getGenreByIdValidator, getGenreById);
route.post("/", cretaeOrUpdateGenreValidator, createGenre);
route.put("/:id", updateGenreById);
route.delete("/:id", deleteGenreById);

module.exports = route;
