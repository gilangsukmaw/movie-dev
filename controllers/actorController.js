const { actor } = require("../models/model");

class ActorController {
  getAllActor = async (req, res, next) => {
    try {
      const data = await actor.find().select("id name photo");
      const total = data.length;
      return res.status(200).json({ total, data });
    } catch (error) {
      next(error);
    }
  };
  getById = async (req, res, next) => {
    try {
      const data = await actor
        .findOne({ _id: req.params.id })
        .select("-deleted -__v")
        .populate("movies", "title");
      return res.status(200).json({ data });
    } catch (error) {
      next(error);
    }
  };
}

module.exports = new ActorController();
